# Create A Chatroom From Scratch PHP and MySQLi #

Prerequisites

- Basic HTML
- Basic CSS
- Basic PHP syntax
- Basic MySQLi


## What You Will Learn ##

- How to create a loging page
- How to check whether a login credential is correct
- How to add data to a table
- How to check for duplicate users in table
- how to create chatroom interface
- How to add text to chatroom
- How to display text in a chatroom
- How to let the user sign out

## Isset() vs !Empty() ##

- Isset() blocks out anything set to NULL
- All unset variables are NULL by default
- All $_SESSION cookies, if they have not been explicitly set, are NULL
- This includes if you have no session started at all
- Isset does NOT block "/"", the defalut value of an input in a form if you don't fill in anything.
- What to do if you wist to block out "?
- Original function is empty(), !means not
- !empty() blocks out NULL as well as "/""
- Possible problem: it alse block out 0

## MySQL Review ##

- Will cover the purpose and basics syntax of SELECT, INSERT, UPDATE and DELETE in MySQL.

### Select statement: ###

- Goes into your table and gets a bunch of records that you specify
- Select * from table where column1 = value1, column2=value2, column3=value3

### insert statement ###

- Adds a new set of records to our table
- Insert into table_name(column1, column2, column3) Value(value1, value2,value3)

### Update statement ###

- Used to change the records of a row to something else
- update table_name set column1=value1, column2=value2, ... where condition

### Delete statement ###

- deletes row(s) from table that have record equal to value_1 in column_1
- Delete from table_name wheree condition

### Closing your statement and connection ###


```
$statement->close();
$conn->close();

```
### SQL Information ###

Database name: chat_room

Two tables: chat and user

Chat Table:

- Username(varchar(255))
- Dialogue(varchar(255))
- Time(varchar(255))

User Table:

- Username(varchar(255))
- Password(varchar(255))

Chatroom: Key features of Project

- Login page
- Check login page
- Relogin page
- Add user page
- Duplicate user page
- Check user page
- Chatroom main page
- logout page

#### Login page ####

- This is the initial screen where the user signs in 
- The user can input his/her username and password
- We use of form, two inputs for the user to enter text, and a submit button
- Data in the inputs are submitted to check_login.php which confirms whether password is correct

#### Check login page ####

- We send POST data to this screen
- This is the value of the "action" attribute
- we do threee things: go into the user table, get the password of the row that has out corresponding user value, and then compare the password with what is in the table
- If the password values match, we processes to the Chat Main screen. Otherwise, we ask the user to relogin

#### Relogin login page ####

- If the user's username and password don't match, they are feferred to relogin.php
- This is essetially the same screen as the login screen
- The user may reenter their username and password and this is sent over to check_login.php again

#### Add user page ####

- This screen let's us add aser to our user table
- We enter a username and password combination via form and two input tags (along with a button)
- The data is sent off to the check_user.php page to be checked for duplicate usernames.

#### Duplicate user page ####

- We sent the user to this page if a user name already exists in out user table
- Same code as add_user.php essentially

#### Check user page ####

- The add user screen sends POST data to this screen to check whether a username and password combinations exists
- IF no such user name exists, we use SQL to add the user and password combination into our user table
- If a username is already in our database, we sent the user to a duplicate user screen.
 
#### Chat main page ####

- When the user successful logs in, we bring him/her to this page
- This is the most complicated part
- Essentially, they user types in text in a input field and the text is displayed on the page
- However after the user has entered a set amount of text, the text automaticallu deletes iteself

#### Logout page ####

- Sins on the user by ending the session
- Sets all the $_SESSION variables to NULL and destroy the session itself




























