
<?php

    $DBHOST = "localhost";
    $DBNAME = "test_db";
    $DBPWD = "";
    $DBUSER = "root";


    $conn = new mysqli($DBHOST, $DBUSER, $DBPWD, $DBNAME);

    if($conn->connect_error)
    {
        die("Connection failed: " . $conn->connect_error);
    }

    $first_name=$_POST["first_name"];
    $last_name=$_POST["last_name"];

    //echo "Connect successfully";
    $statement = "INSERT INTO name VALUES(?,?)";

   $stmt =  $conn->prepare($statement);  
   $stmt->bind_param("ss",$first_name,$last_name); 
   $stmt->execute();
   $stmt->close();

   $conn->close();

?>