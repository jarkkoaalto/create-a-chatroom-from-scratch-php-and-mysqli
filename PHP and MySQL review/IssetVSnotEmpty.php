<html>
<head><title>Isset vs !empty</title></head>
<body>
<?php

// $a = "hello, world"; // IS set
// $a = NULL; // Is not set
// $a = " "; // IS set
// $a = ''; // Is set
// $a = 0; // IS set


// sesion_start();
// $_SESSION["a"] = "hello world";  //IS set
// $_SESSION["a"] = TRUE;  //IS set 
// $_SESSION["a"] = FALSE; // Is false
// $_SESSION["a"] = "" ; // is set
// $_SESSION["a"] = NULL; // Is not set




// if(isset($x)) // is not set
// if(isset())  // Is set
// {
//    echo "Is set";
// }
// else
// {
//    echo "Is not set";
// }


// $a = "hello world"; // is not empty
// $a = NULL; // is empty
$a =  ""; // Is empty
// $a = ''; // Is empty
// $a = 0; //is empty

// session_start();
// $_SESSION["a"] = ""; // is empty
// $_SESSION["a"] = ''; // is empty


if(!empty)
    {
        echo "Is not empty";
    }
    else
    {
        echo "Is empty";
    }
?>
</body>
</html>