<?php

$DBHOST = "localhost";
$DBUSER = "root";
$DBPWD = "";
$DBNAME = "test_db";


$conn = new mysqli($DBHOST, $DBUSER, $DBPWD, $DBNAME);

$first_name = $_POST["first_name"];
$last_name = $_POST["last_name"];

$statement ="UPDATE name SET last_name=? WHERE first_name=?";

$stmt = $conn->prepare($statement);
$stmt->bind_param("ss",$first_name,$last_name);
$stmt->execute();

$stmt->close();
$conn->close();