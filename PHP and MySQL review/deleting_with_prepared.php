<?php

$DBHOST = "localhost";
$DBUSER = "root";
$DBPWD = "";
$DBNAME = "test_db";

$conn = new mysqli($DBHOST, $DBUSER, $DBPWD, $DBNAME);

if($conn->connect_error)
{
    die("Connection failed: " . $conn->connect_error);
}

$first_name = $_POST["first_name"];
$last_name = $_POST["last_name"];
$statement = "DELETE FROM name WHERE first_name=? AND last_name=?";

$stmt = $conn->prepare($statement);
$stmt->bind_param("ss",$first_name, $last_name);
$stmt->execute();

$stmt->close();
$conn->close();
?>