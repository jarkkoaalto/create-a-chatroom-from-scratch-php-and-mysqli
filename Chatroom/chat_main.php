<html>
<head><title></title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<?php

session_start();
if(isset($_SESSION["username"]))
{

    if(isset($_POST["dialogue"]) && $_POST["dialogue"] !='')
    {

    $DBHOST = "localhost";
    $DBUSER = "root";
    $DBPWD="";
    $DBNAME ="chat_room";

    $conn = new mysqli($DBHOST, $DBUSER, $DBPWD, $DBNAME);

    if($conn->connect_error){
        die("Connection failed!", $conn->$connect_error);
    }

    $username = $SESSION["username"];
    $dialogue = $_POST["dialogue"];
    $time = date("h:i:s");

    $statement = "INSERT INTO chat(username, dialogue, time) VALUES(?,?,?)";
    $stmt = $conn->prepare($statement);
    $stmt->bind_param("sss",$username,$dialogue,$time);
    $stmt->execute();



    $statement = "SELECT * FROM chat WHERE username=?";
    $stmt = $conn->prepare($statement);
    $stmt->bind_param("s",$username);
    $stmt->execute();
    $result = $stmt->get_result();

    while($row = $result->fetch_assoc())
    {
        echo $row["username"];
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        echo $row["dialogue"];
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        echo $row["time"];
        echo "<br>";
    }

    if($result->num_rows>=30){
        $result->data_seek(29);
        $row = $result->fetch_assoc();
        $time = $row["time"];
        $statement = "DELETE FROM chat WHERE time<=?";
        $stmt = $conn->prepare($statement);
        $stmt->bind_param("s", $time);
        $stmt->execute();
    }
    $stmt->close();
    $conn->close();
    }
}
else
{
    header("Location:login.php");
}

echo "<form id='chat_form' action='chat_main.php' method='POST'>";
echo "<input id='chat_dialogue' type='text' name='dialogue'>";
echo "<input id='chat_submit' type='submit' value='submit'>";
echo "</form>";
echo "<a href='exit.php'><button id='exit'>Exit</button></a> ";

?>

</body>
</html>