<?php

 $DBHOST = "localhost";
 $DBNAME = "test_db";
 $DBPWD = "";
 $DBUSER = "root";

 $conn = new mysqli($DBHOST, $DBUSER, $DBPWD, $DBNAME);

 if($conn->connect_error)
 {
     die("Connection failed: " . $conn->connect_error);
 }
//$statement = "SELECT * FROM name WHERE first_name='$first_name'";

  $statement = "SELECT * FROM name";
  $sql = $conn->query($statement);
  
  while($row = $sql->fetch_assoc())
  {
      echo $row["first_name"];
      echo " " ;
      echo $row["last_name"];
      echo "<br>";
  } 

  // OR

  /*
  echo = $sql->fetch_assoc();
  echo $row["first_name"];
  echo " ";
  echo $row["last_name"];
  echo "<br>";
*/
?>